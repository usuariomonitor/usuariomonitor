package com.example.androidclient;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import  java.io.FileOutputStream;
import java.io.BufferedInputStream;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	TextView textResponse;
	EditText editTextAddress, editTextPort;
	Button buttonConnect, buttonClear;

	EditText welcomeMsg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		editTextAddress = (EditText) findViewById(R.id.address);
		editTextPort = (EditText) findViewById(R.id.port);
		buttonConnect = (Button) findViewById(R.id.connect);
		buttonClear = (Button) findViewById(R.id.clear);
		textResponse = (TextView) findViewById(R.id.response);

		welcomeMsg = (EditText) findViewById(R.id.welcomemsg);

		buttonConnect.setOnClickListener(buttonConnectOnClickListener);

		buttonClear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				textResponse.setText("");
			}
		});
	}

	OnClickListener buttonConnectOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {

			String tMsg = welcomeMsg.getText().toString();
			if (tMsg.equals("")) {
				tMsg = null;
				Toast.makeText(MainActivity.this, "No Welcome Msg sent", Toast.LENGTH_SHORT).show();
			}

			MyClientTask myClientTask = new MyClientTask(editTextAddress
					.getText().toString(), Integer.parseInt(editTextPort
					.getText().toString()),
					tMsg);
			myClientTask.execute();
		}
	};

	public class MyClientTask extends AsyncTask<Void, Void, Void> {

		String dstAddress;
		int dstPort;
		String response = "";
		String msgToServer;

		MyClientTask(String addr, int port, String msgTo) {
			dstAddress = addr;
			dstPort = port;
			msgToServer = msgTo;
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			Socket socket = null;
			DataOutputStream dataOutputStream = null;
			DataInputStream dataInputStream = null;

			try {
				socket = new Socket(dstAddress, dstPort);
				dataOutputStream = new DataOutputStream(
						socket.getOutputStream());
				dataInputStream = new DataInputStream(socket.getInputStream());

				if (msgToServer != null) {
					dataOutputStream.writeUTF(msgToServer);
				}

				response = dataInputStream.readUTF();


				//RECIBO DE IMAGENES
				DataInputStream dis = new DataInputStream( socket.getInputStream() );

				// Obtenemos el nombre del archivo
				String nombreArchivo = dis.readUTF().toString();

				// Obtenemos el tamaño del archivo
				int tam = dis.readInt();

				System.out.println( "Recibiendo archivo "+nombreArchivo );

				// Creamos flujo de salida, este flujo nos sirve para
				// indicar donde guardaremos el archivo
				FileOutputStream fos = new FileOutputStream("/sdcard/download/"+nombreArchivo);
				BufferedOutputStream out = new BufferedOutputStream( fos );
				BufferedInputStream in = new BufferedInputStream(socket.getInputStream());

				// Creamos el array de bytes para leer los datos del archivo
				byte[] buffer = new byte[ tam ];

				// Obtenemos el archivo mediante la lectura de bytes enviados
				for( int i = 0; i < buffer.length; i++ )
				{
					buffer[ i ] = ( byte )in.read( );
				}

				// Escribimos el archivo
				out.write( buffer );

				//Leyendo imagen

/*
				String imageInSD = Environment.getExternalStorageDirectory().getAbsolutePath() +"/download/" + nombreArchivo;
				Bitmap bm = BitmapFactory.decodeFile(imageInSD);
				ImageView myImageView = (ImageView)findViewById(R.id.img1);
				myImageView.setImageBitmap(bm);

*/
				/*File sdCard = Environment.getExternalStorageDirectory();

				File directory = new File (sdCard.getAbsolutePath() + "/Pictures");

				File file = new File(directory, "image_name.jpg"); //or any other format supported

				FileInputStream streamIn = new FileInputStream(file);

				Bitmap bitmap = BitmapFactory.decodeStream(streamIn); //This gets the image

				streamIn.close();*/


				//leyendo imagen

				// Cerramos flujos
				out.flush();
				in.close();
				out.close();
				//cs.close();

				//FIN RECIBO DE IMAGENES




			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				response = "UnknownHostException: " + e.toString();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				response = "IOException: " + e.toString();
			} finally {
				if (socket != null) {
					try {
						socket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				if (dataOutputStream != null) {
					try {
						dataOutputStream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				if (dataInputStream != null) {
					try {
						dataInputStream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			textResponse.setText(response);
			super.onPostExecute(result);
		}

	}




}
